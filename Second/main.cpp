#include <iostream>
#include <cstdint>
#include <SFML\System\Time.hpp>
#include "Graphic.h"

void test() {
	sf::RenderWindow okno(sf::VideoMode(320, 240), "Kurs SFML 2.0 - http://cpp0x.pl");
	sf::Clock stoper;
	while(okno.isOpen()) {
		sf::Event event;
		while(okno.pollEvent(event)) {
			if(event.type == sf::Event::Closed)
				okno.close();

		} //while
		okno.clear();

		sf::CircleShape ksztalt(std::sin(stoper.getElapsedTime().asSeconds()) * okno.getSize().y / 8 + okno.getSize().y / 4);
		ksztalt.setOrigin(sf::Vector2f(ksztalt.getRadius(), ksztalt.getRadius()));
		ksztalt.setPosition(okno.getSize().x / 2.0f, okno.getSize().y / 2.0f);
		ksztalt.setFillColor(sf::Color::Yellow);
		okno.draw(ksztalt);

		okno.display();
	} //while
}

void first() {
	sf::RenderWindow window(sf::VideoMode(800, 600, 32), "GAME");
	sf::Event event;

	SFML::Graphic::Texture texture;
	texture.load("../images/grass.png");
	SFML::Graphic::Sprite sprite(texture);

	while(window.isOpen()) {
		window.clear(sf::Color(0, 0, 255));
		while(window.pollEvent(event)) {
			if(event.type == sf::Event::Closed)
				window.close();
			if(sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
				window.close();
		}
		window.draw(sprite.get());
		window.display();
	}
}

void waitSeconds(uint32_t second) {
	sf::Clock clock;
	do {
		sf::Time elapsed1 = clock.getElapsedTime();
		if(second < elapsed1.asSeconds())
			break;
	} while(true);
}

void waitMiliSeconds(uint32_t mili) {
	sf::Clock clock;
	do {
		sf::Time elapsed1 = clock.getElapsedTime();
		if(mili < elapsed1.asMilliseconds())
			break;
	} while(true);
}

void waitMicroSeconds(uint32_t micro) {
	sf::Clock clock;
	do {
		sf::Time elapsed1 = clock.getElapsedTime();
		if(micro < elapsed1.asMilliseconds())
			break;
	} while(true);
}

auto main()->int {
	first();
	return 0;
}

