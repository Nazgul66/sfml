#include <SFML\Graphics.hpp>
#include <memory>
#include <string>

namespace SFML{
	namespace Graphic{
		struct Color {
			Color(uint8_t, uint8_t, uint8_t, uint8_t) {}
			sf::Color get()const { return _color; }
		private:
			sf::Color _color;
		};

		struct Vector2f {
			Vector2f(sf::Vector2f v2f):_vector(v2f) {}
			Vector2f(int x, int y):_vector(x, y) {}
			
			sf::Vector2f get()const { return _vector; }
			inline float x()const { return _vector.x; }
			inline float y()const { return _vector.y; }
		private:
			sf::Vector2f _vector;
		};

		class Texture {
		public:
			bool load(const std::string& patch) { return _texture.loadFromFile(patch.c_str()); }
			sf::Texture get()const { return _texture; }
			inline Vector2f size()const { return Vector2f(_texture.getSize().x, _texture.getSize().y); }
		protected:
		public:
			sf::Texture _texture;
		};


		class Sprite {
		public:
			Sprite(Texture texture, Vector2f vector = Vector2f(0.0f, 0.0f), Color color = Color(0xff, 0xff, 0xff, 0xff))
				:_texture(texture), _color(color), _origin(Vector2f(texture.size().x() / 2, texture.size().y() / 2)), _vector(vector), _sprite(texture.get()) {}
		
			inline void move(float offSetX, float offSetY) { _sprite.move(offSetX, offSetY); }
			inline void move(const Vector2f v2f) { _sprite.move(v2f.get()); }

			sf::Sprite get()const { return _sprite; }
			//Color
			inline void setColor(uint8_t r, uint8_t g, uint8_t b, uint8_t alpha) {
				_color = Color(r, g, b, alpha);
				_sprite.setColor(_color.get());
			}
			inline Color getColor()const { return _color; };

			//position
			inline Vector2f getPosition()const { return _vector; }
			inline void setPosition(float x, float y) {
				_vector = Vector2f(x, y);
				_sprite.setPosition(_vector.get());
			}
			inline void setPosition(const Vector2f v2f) {
				_vector = v2f;
				_sprite.setPosition(v2f.get());
			}

			//origin
			inline Vector2f getOrigin()const { return _sprite.getOrigin(); }
			inline void setOrigin(float x, float y) { _sprite.setOrigin(sf::Vector2f(x, y)); }
			inline void setOrigin(Vector2f v2f) { _sprite.setOrigin(v2f.get()); }

			//rotation
			inline float getRotation()const { return _sprite.getRotation(); }
			inline void setRotation(float angle) { _sprite.setRotation(angle); }

			//texture
			inline SFML::Graphic::Texture getTexture() { return _texture; }
			inline void setTexture(SFML::Graphic::Texture texture) {
				_texture = texture;
				_sprite.setTexture(_texture.get());
			}

			//scale
			inline void setScale(Vector2f v2f) { _sprite.setScale(v2f.get()); }
			inline void setScale(float x, float y) { _sprite.setScale(x, y); }
			inline Vector2f getScale() { return _sprite.getScale(); }

		protected:
		private:
			SFML::Graphic::Texture _texture;
			sf::Sprite _sprite;
			Color _color;
			Vector2f _vector;
			Vector2f _origin;
		};
	}
}